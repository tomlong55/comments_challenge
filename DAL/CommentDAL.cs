using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EL;
using EL.Models;

namespace DAL
{
    public class CommentDAL
    {
        private readonly PostsContext _dbContext;

        public CommentDAL(PostsContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<PostModel> GetPosts(string title = null, string sortOrder = null)
        {
            List<PostModel> postsQuery;
            try
            {
                postsQuery = (from t1 in _dbContext.Posts
                              where t1.Title.Contains(title.ToLower()) || title == null
                              select new PostModel
                              {
                                  Id = t1.PostId,
                                  UserId = t1.UserId,
                                  Title = t1.Title,
                                  Content = t1.Content,
                                  CreatedAt = t1.CreatedAt,
                                  UpdatedAt = t1.UpdatedAt,
                                  Comments = (from t2 in _dbContext.Comments
                                              where t1.PostId == t2.PostId
                                              select new CommentModel
                                              {
                                                  Id = t2.CommentId,
                                                  UserId = t2.UserId,
                                                  PostId = t2.PostId,
                                                  Content = t2.Content,
                                                  CreatedAt = t2.CreatedAt,
                                                  UpdatedAt = t2.UpdatedAt,
                                                  User = (from t3 in _dbContext.Users
                                                          where t2.UserId == t3.UserId
                                                          select new UserModel
                                                          {
                                                              Id = t3.UserId,
                                                              FirstName = t3.FirstName,
                                                              LastName = t3.LastName,
                                                              CreatedAt = t3.CreatedAt,
                                                              UpdatedAt = t3.UpdatedAt
                                                          }).FirstOrDefault(),
                                              }).AsEnumerable(),
                              }).ToList();
            }
            catch (Exception ex)
            {
                return new List<PostModel>();
            }
            if (sortOrder == "oldest")
            {
                return postsQuery.OrderByDescending(t => t.CreatedAt).ToList();
            }
            else
            {
                return postsQuery.OrderBy(t => t.CreatedAt).ToList();
            }

            //throw new NotImplementedException();
        }
    }
}
